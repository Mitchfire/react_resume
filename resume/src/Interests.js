import React, {Component} from 'react';

class Interests extends Component {
  render() {
    return(
        <ul class="list-unstyled interests-list">
          <li>{this.props.item.name}</li>
        </ul>
     
    );
  }
}

export default Interests;