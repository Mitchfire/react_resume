import React, {Component} from 'react';

class Education extends Component {
  constructor (props) {
    super(props);
    
    this.state = {};
  }
  render() {
    return(
      <div className="item">
        <h4 className="degree">{this.props.item.degree}</h4>
        <h5 className="meta">{this.props.item.school}</h5>
        <div className="time">{this.props.item.period}</div>
      </div>
    );
  }
}

export default Education;